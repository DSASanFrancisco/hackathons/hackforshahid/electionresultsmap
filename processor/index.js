const fs = require('fs-extra');
const JSZip = require('jszip');
const turf = require('@turf/turf');
const xlsx = require('node-xlsx').default;

const filename = process.argv[2];
const xlsxFilename = process.argv[3];

var election;
var precinct_shapes;

let ballotMeasuresResults = xlsx.parse(xlsxFilename);
// for now, just take first (2019) results
ballotMeasuresResults = ballotMeasuresResults[0];
console.log("Loaded BallotMeasuresResults");

loadJsonFilesFromZip(
    filename,
    [
      'BallotTypeManifest.json', 'BallotTypeContestManifest.json',
      'CandidateManifest.json', 'Configuration.json', 'ContestManifest.json',
      'CountingGroupManifest.json', 'CvrExport.json', 'PartyManifest.json',
      'PrecinctPortionManifest.json', 'TabulatorManifest.json'
    ])
    .then(function(result) {
      election = result;
      return fs.readJson('./precincts.geojson');
    })
    .then(function(result) {
      precinct_shapes = result;

      var ballots = [];

      console.log("Transforming data...");

      election.CvrExport.forEach(function(session) {
        // if the vote has been modified, use the modified version, otherwise use original
        var entry = session.Modified ? session.Modified : session.Original;
        var precinct_id = entry.PrecinctPortionId;
        entry.Contests.forEach(function(contest) {
          // tabulate the ranked-choice votes
          var ranks = [];

          // only tabulate unambiguous marks which produced votes
          contest.Marks.filter(function(mark) { return !mark.IsAmbiguous && mark.IsVote })
              .forEach(function(mark) { ranks.push(mark); });

          // sort highest-to-lowest rank
          ranks.sort(function(a, b) { return a.Rank - b.Rank; });

          var topChoice;
          if (ranks.length > 0) {
            var firstRankNum = ranks[0].Rank;
            var matching = ranks.filter(function(
                rank) { return rank.Rank == firstRankNum });
            // We may have multiple marks with the same rank.
            if (matching.length == 1) {
              topChoice = ranks[0];
            } else {
              console.warn("Multiple identical ranks")
            }
          }

          // TODO: do we actually need to filter overvotes?
          var last_rank = 0;
          for (var i = 0; i < ranks.length; i++) {
            var rank = ranks[i];
            if (rank.Rank == last_rank) {
              last_rank = rank.Rank;
              ranks.splice(i - 1, ranks.length);
              ranks.push({overvote: true});
              break;
            }
            last_rank = rank.Rank;
          }
          ranks =
              ranks.filter(function(rank) { return rank.overvote != true; });

          ballots.push({
            precinct : precinct_id,
            contest : contest.Id,
            ranks : ranks,
            choice : topChoice,
            counting_type : session.CountingGroupId
          });
        });
      });

      var precincts = election.PrecinctPortionManifest.map(function(precinct) {
        precinct.geometry =
            shapeForPrecinct(election, precinct_shapes, precinct.Id);
        return precinct;
      });

      var final_geometries = [];

      precincts = precincts.map(function(precinct) {
        precinct.Contests = {};

        var precinct_ballots = ballots.filter(function(
            ballot) { return ballot.precinct == precinct.Id; });

        precinct_ballots.forEach(function(pb) {
          if (!precinct.Contests[pb.contest]) {
            precinct.Contests[pb.contest] = {total: 0};
          }

          if (pb.choice && pb.choice.CandidateId) {
            if (!precinct.Contests[pb.contest][pb.choice.CandidateId]) {
              precinct.Contests[pb.contest][pb.choice.CandidateId] = 0;
            }

            // register choice for candidate
            precinct.Contests[pb.contest][pb.choice.CandidateId]++;
            precinct.Contests[pb.contest]["total"]++;
          }
        });

        precinct.Turnout = {};

        const rows = ballotMeasuresResults.data.filter(function(row) { return row[0] == precinct.Description; });
        if (rows.length) {
          precinct.Turnout.Registration = rows[0][3];
          precinct.Turnout.BallotsCast = rows[0][4];
          precinct.Turnout.Turnout = rows[0][5];
        }

        return precinct;
      });

      var transformed = {
        contests : election.ContestManifest,
        candidates : election.CandidateManifest,
        counting_types : election.CountingGroupManifest,
        precincts : precincts
      };

      return fs.writeJson('./output.json', transformed);
    })
    .then(function() { console.log("Done"); });

function shapeForPrecinct(election, precinct_shapes, precint_id) {
  var precinctPortionForVote = election.PrecinctPortionManifest.filter(function(
      x) { return x.Id == precint_id; })[0];

  var geometry = precinct_shapes.features.filter(function(feature) {
    return precinctPortionForVote.Description.indexOf(
               " " + feature.properties.prec_2012) > -1 ||
           precinctPortionForVote.Description.indexOf(
               "/" + feature.properties.prec_2012) > -1
  });

  if (geometry.length > 1) {
    geometry = turf.combine(turf.featureCollection(geometry)).features[0];
  } else {
    geometry = geometry[0];
  }

  return geometry;
}

function loadJsonFilesFromZip(zipFile, filenames) {
  var loaded = 0;
  var result = {};

  return new Promise(function(resolve, reject) {
    fs.readFile(filename, function(err, data) {
      JSZip.loadAsync(data).then(function(zip) {
        filenames.forEach(function(filename) {
          var prettyName = filename.split('.')[0];

          zip.file(filename).async("text").then(function(stringResult) {
            loaded++;
            var json = JSON.parse(stringResult);
            if (json.List) {
              json = json.List;
            }
            if (json.Sessions) {
              json = json.Sessions;
            }
            result[prettyName] = json;
            console.log("Loaded " + prettyName);
            if (loaded == filenames.length) {
              resolve(result);
            }
          })
        });
      });
    });
  });
}
