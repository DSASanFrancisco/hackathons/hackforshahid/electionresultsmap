FROM node:12

# Set an env variable for the location of the app files
ENV APP_HOME=/opt/node/app

# update path to include any installed node module executables
RUN echo "export PATH=$APP_HOME/node_modules/.bin:\$PATH\n" >> /root/.bashrc

# Create a directory for the server app to run from
RUN mkdir -p $APP_HOME

# Add the project files into the app directory
ADD . $APP_HOME

# Switch into processor directory and install node modules
WORKDIR $APP_HOME/processor
RUN npm install

# Switch into static web files project as image working directory
WORKDIR $APP_HOME/viewer

# Install a simple static webserver
RUN npm -g install static-server

# Start up the static server
CMD ["sh", "-c", "static-server -p $PORT"]
